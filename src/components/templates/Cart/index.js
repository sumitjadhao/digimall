import React, { useCallback, useContext, useState } from "react";
import DigiMallContext from "../../App";
import { List, InputNumber, Button, Row, Col } from "antd";
import { NavLink } from "react-router-dom";
import { DeleteTwoTone } from "@ant-design/icons";
import BackButton from "../../atoms/BackButton";

const Cart = (props) => {
  const { globalData, dispatch } = useContext(DigiMallContext);
  const [localStorageState, setLocalStorageState] = useState(
    JSON.parse(localStorage.getItem("globalData"))
  );

  let productKey,
    productPriceObj = {};

  const cartList = () => {
    let listKeys = Object.keys(localStorageState.cartData);
    return listKeys.map((key) => {
      return localStorageState.apiData[key];
    });
  };

  const [listData, setListData] = React.useState(cartList());

  const calProductPrice = () => {
    listData.map((item) => {
      productKey = Object.keys(localStorageState.apiData).find(
        (key) => localStorageState.apiData[key] === item
      );

      productPriceObj[productKey] =
        item.price * localStorageState.cartData[productKey];
    });
    return productPriceObj;
  };

  const [productPrice, setProductPrice] = useState(calProductPrice());

  const calSum = () => {
    let sum = 0;
    for (const [key, value] of Object.entries(productPrice)) {
      sum = sum + value / localStorageState.apiData[key].price;
    }
    return sum;
  };

  React.useEffect(() => {
    props.changeCount("set", calSum());
  }, [globalData]);

  const onChange = (value, productKey) => {
    let obj = {};
    obj[productKey] = value;
    dispatch({
      type: "change",
      product: obj,
    });

    localStorage.setItem("globalData", JSON.stringify(globalData));

    productPriceObj = JSON.parse(JSON.stringify(productPrice));

    if (value === 0) {
      let arr = listData;
      const index = arr.indexOf(localStorageState.apiData[productKey]);
      if (index > -1) {
        arr.splice(index, 1);
      }
      setListData(arr);
      delete productPriceObj[productKey];
      setProductPrice(productPriceObj);
      return;
    }

    productPriceObj[productKey] =
      localStorageState.apiData[productKey].price * value;
    setProductPrice(productPriceObj);
  };

  React.useEffect(() => {
    props.changeCount("set", calSum());
  }, [productPrice]);

  const renderDiscount = (params) => {
    let total = totalPrice()
    let discount = "No discount"
    if (total > 101 && total < 500) discount = 10
    if (total > 500) discount = 20
    let discountValue = Math.round(discount/100 * total)
    return `${discount}%` + ` (${total} - ${discountValue}) = ${total - discountValue}`
  }

  const totalPrice = useCallback(() => Object.values(productPrice).reduce((a, b) => a + b, 0), [productPrice])
  
  return (
    <>
      <Row gutter={[0, 16]}>
        <Col offset={1}>
          <BackButton />
        </Col>
        <Col offset={10}>
          <h1>
            <b>Cart</b>
          </h1>
        </Col>
      </Row>
      {listData.length > 0 ? null : (
        <Row>
          <Col offset={11}>
            <h1> Your cart is empty</h1>
          </Col>
        </Row>
      )}
      <Col offset={1} span={22}>
        <List
          itemLayout="horizontal"
          dataSource={listData}
          renderItem={(item) => {
            let productID = Object.keys(localStorageState.apiData).find(
              (key) => localStorageState.apiData[key] === item
            );

            return (
              <List.Item
                actions={[
                  <Row align="left">
                    <Col>
                      <Button
                        icon={<DeleteTwoTone />}
                        onClick={() => onChange(0, productID)}
                      />
                    </Col>
                  </Row>,
                  <Row align="left">
                    <Col>
                      <InputNumber
                        min={1}
                        max={10}
                        defaultValue={localStorageState.cartData[productID]}
                        onChange={(value) => onChange(value, productID)}
                      />
                    </Col>
                  </Row>,
                  <Row align="left">
                    <Col>
                      <b>{"₹ " + productPrice[productID]}</b>{" "}
                    </Col>
                  </Row>,
                ]}
              >
                <List.Item.Meta
                  title={item.item_name}
                  description={
                    "Price: ₹ " +
                    item.price
                  }
                />
              </List.Item>
            );
          }}
        />
      </Col>
      <br />
      <br />
      <Row>
        <Col offset={16}>
          <b>Total Number of Products: </b>
        </Col>
        <Col offset={3}>{calSum()}</Col>
      </Row>
      <Row>
        <Col offset={16}>
          <b>Price: </b>{" "}
        </Col>
        <Col offset={3}>
          {"₹ " + totalPrice()}
        </Col>
      </Row>
      <Row>
        <Col offset={16}>
          <b>Discount Price: </b>{" "}
        </Col>
        <Col offset={3}>
          {renderDiscount()}
        </Col>
      </Row>
    </>
  );
};

export default Cart;
