import React, { useEffect } from "react";
import { List, Col, InputNumber, Row, Button } from "antd";
import { NavLink } from "react-router-dom";
import { apiMock } from "../../../api";
import DigiMallContext from "../../App";

async function fetchAPI() {
    return await fetch("https://run.mocky.io/v3/9d71cb03-a9f9-4d70-bae2-9d3adaa1cfe7")
    .then((r) => r.json())
    .catch((error) => console.log("ERROR", error.message));
}

const Home = (props) => {
  const { globalData, dispatch } = React.useContext(DigiMallContext);

  const [listData, setListData] = React.useState([]);
  const [localStorageState, setLocalStorageState] = React.useState(globalData);
  const [cartData, setCartData] = React.useState({});

  React.useEffect(() => {
    fetchAPI()
      .then((data) => {
          let obj = {}
          for (let i = 0; i < data.length; i++) {
              obj[i] = data[i]
          }
          console.log('objobj', obj)
        let localData = JSON.parse(localStorage.getItem("globalData")) || {};
        localData.apiData = data;
        if (!localData.cartData) {
          localData.cartData = {};
        }

        dispatch({
          type: "fetch",
          product: localData,
        });

        localStorage.setItem("globalData", JSON.stringify(localData));
        props.changeCount(
          "set",
          Object.values(localData.cartData).reduce((a, b) => a + b, 0)
        );
        setListData(Object.values(localData.apiData));
        setLocalStorageState(localData);
        setCartData(localData.cartData);
      })
      .catch((error) => console.log(error));
  }, [dispatch]);

  const renderAction = (productKey) => {
    if (cartData[productKey] >= 0) {
      return (
        <InputNumber
          min={0}
          max={10}
          defaultValue={cartData[productKey]}
          onChange={(value) => onChange(value, productKey)}
        />
      );
    } else {
      return (
        <Button onClick={() => onChange(1, productKey)}>Add to Cart</Button>
      );
    }
  };

  const onChange = (value, productKey) => {
    let obj = {};
    obj[productKey] = value;
    dispatch({
      type: "change",
      product: obj,
    });

    localStorage.setItem("globalData", JSON.stringify(globalData));
    let cartObj = Object.assign({}, cartData);
    if (value === 0) {
      delete cartObj[productKey];
    } else {
      cartObj[productKey] = value;
    }
    setCartData(cartObj);
  };

  React.useEffect(() => {
    props.changeCount(
      "set",
      Object.values(cartData).reduce((a, b) => a + b, 0)
    );
  }, [cartData]);

  return (
    <>
      <title>Home</title>
      <Col offset={10}>
        <h1>
          <b>Welcome to DigiMall !</b>
        </h1>
      </Col>
      <br />
      <br />
      <List
        itemLayout="horizontal"
        loading={listData.length > 0 ? false : true}
        dataSource={listData}
        renderItem={(item) => {
          let productID = Object.keys(localStorageState.apiData).find(
            (key) => localStorageState.apiData[key] === item
          );
          return (
            <List.Item
              actions={[
                <Row align="left">
                  <Col>{renderAction(productID)}</Col>
                </Row>,
              ]}
            >
              <List.Item.Meta
                title={item.item_name}
                description={"₹ " + item.price}
              />
            </List.Item>
          );
        }}
      />
    </>
  );
};

export default Home;
